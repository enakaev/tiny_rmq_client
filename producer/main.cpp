/// @file
/// @brief
/// @copyright Copyright (c) InfoTeCS. All Rights Reserved.

#include <stdexcept>
#include <iostream>

#include <boost/program_options/options_description.hpp>
#include <boost/program_options/parsers.hpp>
#include <boost/program_options/value_semantic.hpp>
#include <boost/program_options/variables_map.hpp>
#include <boost/exception/diagnostic_information.hpp>

#include <client/simple/simple_client.h>


int main( int argc, char** argv )
{
     using edi::ts::rabbitmq::client::simple::Connection;
     using edi::ts::rabbitmq::client::simple::SimpleClient;

     try
     {
          std::string rmqHost = "localhost";
          std::uint16_t rmqPort = 5672;
          std::string vhost = "/";
          std::string user;
          std::string password;

          std::string exchange;
          std::string routingKey;
          std::string message;

          boost::program_options::options_description description { "Options" };
          description.add_options()
               ( "help", "show this help and quit" )
               ( "host,H", boost::program_options::value< decltype( rmqHost ) >( &rmqHost )->required()->default_value( rmqHost ), "RabbitMQ server hostname or IP" )
               ( "port,P", boost::program_options::value< decltype( rmqPort ) >( &rmqPort )->implicit_value( rmqPort ), "RabbitMQ listening port" )
               ( "vhost,V", boost::program_options::value< decltype( vhost ) >( &vhost )->required()->default_value( vhost ), "RabbitMQ virtual hostname"  )
               ( "user,U", boost::program_options::value< decltype( user ) >( &user )->required(), "RabbitMQ user's login" )
               ( "password,S", boost::program_options::value< decltype( password ) >( &password )->required(), "RabbitMQ user's secret password" )
               ( "exchange,E", boost::program_options::value< decltype( exchange ) >( &exchange ), "Exchange name for sending messages"  )
               ( "rounting-key,R", boost::program_options::value< decltype( routingKey ) >( &routingKey ), "Routing key for sending message"  )
               ( "message,M", boost::program_options::value< decltype( message ) >( &message )->required(), "Sending message" )
               ;
          boost::program_options::variables_map options;
          boost::program_options::store(
               boost::program_options::parse_command_line( argc, argv, description )
               , options
               );
          if( options.count( "help" ) )
          {
               std::cout << description << '\n';
               return EXIT_SUCCESS;
          }
          boost::program_options::notify( options );

          std::cout << "RabbitMQ on " << rmqHost << ':' << rmqPort
               << " (vhost: " << vhost << ", user: " << user << ")\n";

          const Connection::Parameters params{ rmqHost, rmqPort, user, password, vhost };
          Connection connection{ params };

          std::cout << "Publish message " << std::quoted( message )
               << " to exchange " << std::quoted( exchange )
               << " using routing key " << std::quoted( routingKey ) << "... ";

          SimpleClient::publishMessage( connection, exchange, routingKey, message );

          std::cout << "ok.\n";
     }
     catch( const std::exception& e )
     {
          std::cerr << "exception: " << boost::diagnostic_information( e ) << '\n';
          return 1;
     }

     return 0;
}
