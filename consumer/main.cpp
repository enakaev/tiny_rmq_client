/// @file
/// @brief
/// @copyright Copyright (c) InfoTeCS. All Rights Reserved.

#include <csignal>
#include <stdexcept>
#include <iostream>
#include <initializer_list>

#include <boost/optional/optional.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/parsers.hpp>
#include <boost/program_options/value_semantic.hpp>
#include <boost/program_options/variables_map.hpp>
#include <boost/exception/diagnostic_information.hpp>

#include <client/simple/error.h>
#include <client/simple/simple_client.h>


void startListener(
     const std::string& hostname
     , const int port
     , const std::string& username
     , const std::string& password
     , const std::string& virtualHost
     , const std::string& exchange
     , const std::string& queueName
     , const boost::optional< boost::posix_time::time_duration >& timeout = boost::none
)
{
     using edi::ts::rabbitmq::errors::ConnectionError;
     using edi::ts::rabbitmq::client::simple::Connection;
     using edi::ts::rabbitmq::client::simple::SimpleClient;

     const Connection::Parameters params(
          hostname,
          port,
          username,
          password,
          virtualHost
     );

     Connection connection( params );
     bool reconnect = false;
     while( true )
     {
          try
          {
               if( reconnect )
               {
                    connection.reconnect();
                    reconnect = false;
               }

               SimpleClient::bind( connection, exchange, queueName );
               while( true )
               {
                    const auto& env = SimpleClient::consumeMessage( connection, timeout );
                    if( env )
                    {
                         std::cout << "Got message:\n" << env->message << "\n";
                         SimpleClient::ackMessage( connection, *env );
                    }
                    else
                    {
                         std::cout << "No message consumed.\n";
                    }
               }
          }
          catch( const ConnectionError& e )
          {
               std::cerr << "connection error: " << e.what() << "\n";
               reconnect = true;
          }
          catch( const std::runtime_error& e )
          {
               std::cerr << "exception: " << boost::diagnostic_information( e ) << "\n";
               return;
          }
     }
}


int main( int argc, char** argv )
{
     try
     {
          std::string rmqHost = "localhost";
          std::uint16_t rmqPort = 5672;
          std::string vhost = "/";
          std::string user;
          std::string password;

          std::string exchange;
          std::string queueName;

          boost::program_options::options_description description { "Options" };
          description.add_options()
               ( "help", "show this help and quit" )
               ( "host,H", boost::program_options::value< decltype( rmqHost ) >( &rmqHost )->required()->default_value( rmqHost ), "RabbitMQ server hostname or IP" )
               ( "port,P", boost::program_options::value< decltype( rmqPort ) >( &rmqPort )->implicit_value( rmqPort ), "RabbitMQ listening port" )
               ( "vhost,V", boost::program_options::value< decltype( vhost ) >( &vhost )->required()->default_value( vhost ), "RabbitMQ virtual hostname"  )
               ( "user,U", boost::program_options::value< decltype( user ) >( &user )->required(), "RabbitMQ user's login" )
               ( "password,S", boost::program_options::value< decltype( password ) >( &password )->required(), "RabbitMQ user's secret password" )
               ( "exchange,E", boost::program_options::value< decltype( exchange ) >( &exchange ), "Exchange name for sending messages"  )
               ( "queue-name,Q", boost::program_options::value< decltype( queueName ) >( &queueName ), "Queue name for listening"  )
               ;
          boost::program_options::variables_map options;
          boost::program_options::store(
               boost::program_options::parse_command_line( argc, argv, description )
               , options
               );
          if( options.count( "help" ) )
          {
               std::cout << description << '\n';
               return EXIT_SUCCESS;
          }
          boost::program_options::notify( options );

          startListener(
               rmqHost
               , rmqPort
               , user
               , password
               , vhost
               , exchange
               , queueName
               , boost::posix_time::milliseconds{ 300 }
               );
     }
     catch( const std::exception& e )
     {
          std::cerr << "exception: " << boost::diagnostic_information( e ) << '\n';
          return EXIT_FAILURE;
     }

     return EXIT_SUCCESS;
}
