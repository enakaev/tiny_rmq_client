###################################################################################################
##
##   Copyright (C) 2021, Enakaev Aleksey <enakaev.aleksey@yandex.ru>
##
##   Absolutely freeware.
##
###################################################################################################
#
# RabbitMQ cmake module
# (from here https://gitlab.kitware.com/cmake/community/-/wikis/doc/cmake/dev/Module-Maintainers)
#
# Variables:
# - RabbitMQ_FOUND
# - RabbitMQ_INCLUDE_DIR
# - RabbitMQ_LIBRARIES
#

find_package(PkgConfig)
pkg_check_modules(PC_RabbitMQ QUIET RabbitMQ)

find_path(RabbitMQ_INCLUDE_DIR
     NAMES amqp.h
     PATH ${PC_RabbitMQ_INCLUDE_DIRS}
     )
find_library(RabbitMQ_LIBRARY
     NAMES rabbitmq
     PATHS ${PC_RabbitMQ_LIBRARY_DIRS}
     )

# Хоть библиотека всего одна, имя переменной делаем во множ. числе по следующим причинам:
# - для единообразия с другими переменными (например, Boost_..._LIBRARIES);
# - на случай, если в следующих версиях librabbitmq появятся дополнительные библиотеки.
#
set(RabbitMQ_LIBRARIES ${RabbitMQ_LIBRARY})

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(RabbitMQ
     REQUIRED_VARS
          RabbitMQ_LIBRARIES
          RabbitMQ_INCLUDE_DIR
     )

mark_as_advanced(
     ${RabbitMQ_LIBRARIES}
     ${RabbitMQ_INCLUDE_DIR}
     )