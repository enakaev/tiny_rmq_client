/// @file
/// @brief
/// @copyright Copyright (c) InfoTeCS. All Rights Reserved.

#pragma once

#include <stdexcept>
#include <string>
#include <amqp.h>


namespace edi {
namespace ts {
namespace rabbitmq {
namespace errors {


/// Тип исключения, генерируемый при ошибках соединения
struct ConnectionError : std::runtime_error
{
     ConnectionError( const std::string& msg ) : std::runtime_error( msg ) {}
};


/// Функции разбирают ошибки и генерируют исключения
/// @throws ConnectionError, std::runtime_error
void ensureNoErrors( int status, const std::string& context );


/// Функции разбирают ошибки и генерируют исключения
/// @throws ConnectionError, std::runtime_error
void ensureNoErrors( amqp_status_enum status, const std::string& context );


/// Функции разбирают ошибки и генерируют исключения
/// @throws ConnectionError, std::runtime_error
void ensureNoErrors( const amqp_rpc_reply_t& reply, const std::string& context );



} // namespace errors
} // namespace rabbitmq
} // namespace ts
} // namespace edi
