/// @file
/// @brief
/// @copyright Copyright (c) InfoTeCS. All Rights Reserved.

#include <client/simple/utils.h>


namespace edi {
namespace ts {
namespace rabbitmq {
namespace utils {


std::string toString( const amqp_bytes_t& bytes )
{
     return std::string( static_cast< const char* >( bytes.bytes ), bytes.len );
}


amqp_bytes_t fromString( const std::string& str )
{
     return str.empty() ? amqp_empty_bytes : amqp_cstring_bytes( str.c_str() );
}


} // namespace utils
} // namespace rabbitmq
} // namespace ts
} // namespace edi
