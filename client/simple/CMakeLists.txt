set(NAME rmqclientsimple)

add_library(${NAME}
    src/error.cpp
    src/utils.cpp
    src/simple_client.cpp
)

target_link_libraries(${NAME}
    ${RabbitMQ_LIBRARIES}
)